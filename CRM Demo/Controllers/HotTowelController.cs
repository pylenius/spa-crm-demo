using System.Web.Mvc;

namespace CRM_Demo.Controllers
{
    public class HotTowelController : Controller
    {
        //
        // GET: /HotTowel/

        public ActionResult Index()
        {
            var identity = User.Identity;
            if (identity.IsAuthenticated)
            {

                var vm = new UserSelfViewModel
                             {
                                 UserName = identity.Name
                             };
                var info = Newtonsoft.Json.JsonConvert.SerializeObject(vm);
                ViewBag.UserInfo = info;
            }
            return View();
        }

    }
}

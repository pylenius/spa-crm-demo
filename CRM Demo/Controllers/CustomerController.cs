﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using Breeze.WebApi;
using CRM_Demo.Models;
using Newtonsoft.Json.Linq;

namespace CRM_Demo.Controllers
{

    public class CustomerDataProvider : EFContextProvider<CustomerDataContext>
    {
        protected override bool BeforeSaveEntity(EntityInfo entityInfo)
        {
            // return false if we don’t want the entity saved.
            // prohibit any additions of entities of type 'Role'
            if (entityInfo.Entity.GetType() == typeof(CustomerItem)
              && entityInfo.EntityState == EntityState.Added)
            {
                var customer = (CustomerItem) entityInfo.Entity;
                Debug.WriteLine(customer.Name);

                return true;
            }
            else
            {
                return true;
            }
        }

        protected override Dictionary<Type, List<EntityInfo>> BeforeSaveEntities(Dictionary<Type, List<EntityInfo>> saveMap)
        {
            // return a map of those entities we want saved.
            return saveMap;
        }
    }


    [BreezeController]
    [System.Web.Mvc.Authorize]
    public class CustomerDataController : ApiController
    {

        readonly CustomerDataProvider _contextProvider = new CustomerDataProvider();

        [HttpGet]
        public string Metadata()
        {
            return _contextProvider.Metadata();
        }

        [HttpPost]
        public SaveResult SaveChanges(JObject saveBundle)
        {
            return _contextProvider.SaveChanges(saveBundle);
        }

        [HttpGet]
        public IQueryable<CustomerItem> Customers()
        {
            return _contextProvider.Context.Customers;
        }
    }
}

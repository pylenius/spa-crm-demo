﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CRM_Demo.Filters;
using CRM_Demo.Models;
using Facebook;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;

namespace CRM_Demo.Controllers
{
    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class FacebookLoginModel
    {
        [Required]
        [Display(Name = "User ID")]
        public string UserID { get; set; }

        [Required]
        [Display(Name = "AccessToken")]
        public string AccessToken { get; set; }

        [Display(Name = "ExpiresIn")]
        public int ExpiresIn { get; set; }
    }

    public class UserSelfViewModel
    {
        public string UserName { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Company { get; set; }

        public string FacebookToken { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }
    }

    [InitializeSimpleMembership]
    public class UserController : Controller
    {

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult JsonRegister(RegisterModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!string.IsNullOrEmpty(model.FacebookToken))
                    {
                        // Adding Facebook user
                        using (var db = new CustomerDataContext())
                        {
                            UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
                            // Check if user already exists
                            if (user == null)
                            {
                                var client = new FacebookClient(model.FacebookToken);
                                dynamic result = client.Get("me", new { fields = "name,id" });
                                string facebookId = result.id;

                                // Insert name into the profile table
                                db.UserProfiles.Add(new UserProfile { UserName = model.UserName });
                                db.SaveChanges();

                                OAuthWebSecurity.CreateOrUpdateAccount("facebook", facebookId, model.UserName);
                                OAuthWebSecurity.Login("facebook", facebookId, createPersistentCookie: false);

                                return Json(new
                                {
                                    success = true,
                                    userinfo = new UserSelfViewModel
                                    {
                                        UserName = model.UserName
                                    }
                                });
                            }
                            else
                            {
                                return Json(new
                                {
                                    success = false,
                                    message = "Username is already reserved!"
                                });                                
                            }
                        }
                    }
                    else
                    {
                        // Adding normal user
                        WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                        WebSecurity.Login(model.UserName, model.Password);
                    }

                    return Json(new
                                    {
                                        success = true,
                                        userinfo = new UserSelfViewModel
                                                       {
                                                           UserName = model.UserName
                                                       }
                                    });

                }
                catch (MembershipCreateUserException e)
                {
                    return Json(new { success = false, message = AccountController.ErrorCodeToString(e.StatusCode) });
                }
            }

            return Json(new { success = false, message = "UserName missing or password not 6 or more characters"});

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Login(LoginModel model, string returnUrl)
        {
            var db = new CustomerDataContext();

            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {

                return Json(new
                                {
                                    success = true,
                                    userinfo = new UserSelfViewModel
                                                   {
                                                       UserName = model.UserName
                                                   }
                                });
            }
            else
            {
                return Json(new
                                {
                                    success = false,
                                    errors = "" //GetErrorsFromModelState()
                                });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return Json(new
            {
                success = true
            });
        }


        public ActionResult LoginInfo()
        {
            return PartialView("_logininfo");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult FacebookLogin(FacebookLoginModel model, string returnUrl)
        {
            //var accessToken = model.AccessToken;// Session["AccessToken"].ToString();
            var client = new FacebookClient(model.AccessToken);
            dynamic result = client.Get("me", new { fields = "name,id" });
            string name = result.name;
            string facebookId = result.id;

            if (OAuthWebSecurity.Login("facebook", facebookId, true))
            {
                var name2 = OAuthWebSecurity.GetUserName("facebook", facebookId);

                return Json(new
                {
                    success = true,
                    userinfo = new UserSelfViewModel
                    {
                        UserName = name2
                    }
                });
            }

            if (User.Identity.IsAuthenticated)
            {
                // If the current user is logged in add the new account
                OAuthWebSecurity.CreateOrUpdateAccount("facebook", facebookId, User.Identity.Name);

                return Json(new
                {
                    success = true,
                    userinfo = new UserSelfViewModel
                    {
                        UserName = User.Identity.Name
                    }
                });
            }
            else
            {
                dynamic fullResult = client.Get("me", new { fields = "username,email,first_name,last_name" });

                //User needs to register
                return Json(new
                {
                    success = false,
                    registerInfo = new
                    {
                        UserName = fullResult.username,
                        EmailAddress = fullResult.email,
                        FirstName = fullResult.first_name,
                        LastName = fullResult.last_name,
                        FacebookToken = model.AccessToken
                    }
                });
            }
        }
    }
}

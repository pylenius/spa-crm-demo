﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CRM_Demo.Models
{
    public class User
    {
        public int Id { get; set; }
        public bool IsLockedOut { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Company { get; set; }

        public string Street { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }

        public List<NoteItem> Notes { get; set; }

        public bool TryLogin(string password)
        {
            return true;
        }
    }

    public class CustomerItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public List<NoteItem> Notes { get; set; }
    }

    public class NoteItem
    {
        public int Id { get; set; }
        public CustomerItem Customer { get; set; }
        public int CustomerItemId { get; set; }
        public string Text { get; set; }
        public DateTime Added { get; set; }
    }


    public class CustomerDataContext : DbContext
    {
        public CustomerDataContext()
            : base("DefaultConnection")
        {

        }
        static CustomerDataContext()
        {
            Database.SetInitializer(new CustomerDatabaseInitializer());
        }
        public DbSet<CustomerItem> Customers { get; set; }
        public DbSet<NoteItem> Notes { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
    }

    public class CustomerDatabaseInitializer : DropCreateDatabaseAlways<CustomerDataContext>
    {
        protected override void Seed(CustomerDataContext context)
        {
            Array.ForEach(new[] 
            { 
                new CustomerItem{Name = "Microsoft", Street = "Street 1", City = "Helsinki", PostCode = "00123"},
                new CustomerItem{Name = "Nokia", Street = "Street 2", City = "Turku", PostCode = "11123"},
                new CustomerItem{Name = "Apple", Street = "Street 3", City = "Tampere", PostCode = "22123"},
                new CustomerItem{Name = "Samsung", Street = "Street 4", City = "Pori", PostCode = "00123"},
                new CustomerItem{Name = "HTC", Street = "Street 5", City = "Kuusamo", PostCode = "00123"}
            }, i => context.Customers.Add(i));

            context.SaveChanges();
        }
    }
}
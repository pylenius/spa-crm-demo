﻿define(['services/logger', 'viewmodels/account/register', 'durandal/plugins/router', 'services/authentication'], function (logger, register, router, authentication) {
    var vm = {
        facebook: null,
        initialized: ko.observable(false),
        name: ko.observable(""),
        picturesrc: ko.observable(""),
        login: login,
        logout: logout
    };

    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_UK/all.js', function () {
        window.fbAsyncInit = function () {
            vm.facebook = FB;
            vm.facebook.init({
                appId: '165465516809499',
                status: true,
                cookie: true,
                xfbml: true,
                oauth: true
            });
            vm.initialized(true);
            vm.facebook.getLoginStatus(updateLoginStatus);
            vm.facebook.Event.subscribe('auth.statusChange', updateLoginStatus);
        };
    });    
    return vm;
    
    function login() {
        vm.facebook.login(function (response) {
            updateLoginStatus(response);
        } , { scope: 'email' });
    }
    
    function logout() {
        vm.facebook.logout( function (response) {
            vm.picturesrc("");
            vm.name("");
        });
    }

    function updateLoginStatus(response) {
        if (response.authResponse) {
            logger.log("Authenticated to Facebook succesfully");
            
            var userInfo = {
                UserID: response.authResponse.userID,
                AccessToken: response.authResponse.accessToken,
                ExpiresIn: response.authResponse.expiresIn
            };
            var jqxhr = $.post("/user/facebooklogin", userInfo)
				.done(function (data) {
				    if (data.success == true) {
				        authentication.authenticationDone(data.userinfo,"#/");
				        return true;
				    } else if (data.registerInfo) {
				        register.userName(data.registerInfo.UserName);
				        register.firstName(data.registerInfo.FirstName);
				        register.lastName(data.registerInfo.LastName);
				        register.emailAddress(data.registerInfo.EmailAddress);
				        register.facebookToken(data.registerInfo.FacebookToken);
				        router.navigateTo("#/account/register");
				        return true;
				    } else {
				        return data;
				    }
				})
				.fail(function (data) {
				    return data;
				});
            
            //vm.facebook.api('/me', function (response2) {
            //    vm.picturesrc('src="https://graph.facebook.com/' +
            //        +response2.id + '/picture"');
            //    vm.name(response2.name);
                

        } else {
            logger.log("Not authenticated to Facebook");
            vm.picturesrc("");
            vm.name("");
        }
    }

});
﻿define(['durandal/system', 'durandal/app', 'durandal/plugins/router', 'services/antiforgery'], function(system, app, router, antiforgery) {
    var vm = {
        handleUnauthorizedAjaxRequests: handleUnauthorizedAjaxRequests,
        userInfo: {
            UserName: ko.observable("anonymous")
        },
        setUserInfo: setUserInfo,
        resetUserInfo: resetUserInfo,
        isAnonymous: ko.observable(true),
        reloadLoginInfo: reloadLoginInfo,
        authenticationDone: authenticationDone,
        login: login,
        logout: logout
    };

    if (typeof window.InitialUserInfo === 'undefined') {
        ;
    } else {
        vm.setUserInfo(window.InitialUserInfo);
    }
    return vm;


    function handleUnauthorizedAjaxRequests(callback) {
        if (!callback) {
            return;
        }
        $(document).ajaxError(function(event, request, options) {
            if (request.status === 401) {
                callback();
            }
        });
    }

    function canLogin() {
        return true;
    }

    function reloadLoginInfo() {
        $.ajax({
            type: "GET",
            url: "/user/LoginInfo/",
            success: function(viewHtml) {
                $("#__AjaxAntiForgeryForm").replaceWith(viewHtml);
                antiforgery.updateToken();
            }
            //,error: function (errorData) { onError(errorData); }
        });
    }

    function authenticationDone(userInfo, navigateToUrl) {
        //Save userinfo
        setUserInfo(userInfo);
        reloadLoginInfo();

        if (!!navigateToUrl) {
            router.navigateTo(navigateToUrl);
        }
        return true;
    }

    function setUserInfo(info) {
        vm.userInfo.UserName(info.UserName);
        vm.isAnonymous(false);
    }
    function resetUserInfo() {
        vm.userInfo.UserName("anonymous");
        vm.isAnonymous(true);
    }
    function login(userInfo, navigateToUrl) {
        var jqxhr = $.post("/user/login", userInfo)
            .done(function (data) {
                if(data.success == true){
                    return authenticationDone(data.userinfo, navigateToUrl);
                } else {
                    return false;
                }
            })
            .fail(function (data) {
                return data;
            });

        return jqxhr;
    }
    function logout(navigateToUrl) {
        var context = this;
        $.post("/user/logoff", "i=i")
            .done(function(data) {
                if (data.success == true) {
                    //Save userinfo
                    context.resetUserInfo(data.userinfo);
                    context.reloadLoginInfo();

                    if (!!navigateToUrl) {
                        router.navigateTo(navigateToUrl);
                    } 
                }
            })
            .fail(function(data) {                    
            });
    }
    
});
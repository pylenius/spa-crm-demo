﻿define(['services/logger', 'services/authentication', 'durandal/plugins/router', 'i18n!nls/mainloc'], function (logger, authentication, router, mainloc) {
    var vm = {
        activate: activate,
        title: 'Register',
        facebookToken: ko.observable(""),
        userName: ko.observable(),
        firstName: ko.observable(),
        lastName: ko.observable(),
        emailAddress: ko.observable(),
        company: ko.observable(),
        password1: ko.observable(),
        password2: ko.observable(),
        errorMessage: ko.observable(),
        localize: mainloc,
        processing: ko.observable(false),
        registerUser: function () {
            vm.processing(true);
            var d = {
                'FirstName': vm.firstName,
                'UserName': vm.userName,
                'LastName': vm.lastName,
                'EmailAddress': vm.emailAddress,
                'Company': vm.company,
                'Password': vm.password1,
                'ConfirmPassword': vm.password2,
                'FacebookToken': vm.facebookToken
            };
            $.ajax({
                url: 'User/JsonRegister',
                type: "POST",
                data: d,
                success: function (result) {
                    vm.processing(false);                    
                    if (result.success) {
                        vm.errorMessage("");
                        authentication.setUserInfo(result.userinfo);
                        authentication.reloadLoginInfo();
                        router.navigateTo("#/");
                    } else {
                        vm.errorMessage(result.message);
                    }
                },
                error: function (result) {
                    vm.processing(false);
                    vm.errorMessage("Error");
                }
            });
        },
    };


    return vm;

    //#region Internal Methods
    function activate() {
        logger.log('Login Screen Activated', null, 'login', true);
        return true;
    }
    //#endregion
});
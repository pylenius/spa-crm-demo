﻿define(['services/authentication', 'i18n!nls/mainloc', "services/facebook"], function (authenticationService, mainloc, facebook) {
		    var loginVM = function () {
		        var self = this;

		        self.title = mainloc.login;
		        self.userName = ko.observable();
		        self.password = ko.observable();
		        self.rememberMe = ko.observable();
		        self.$errors = ko.observableArray();
		        self.$success = ko.observable();
		        self.localize = mainloc;
		        self.facebook = facebook;
		        
		        self.login = function() {
		            var userInfo = {
		                userName: self.userName(),
		                password: self.password(),
		                rememberMe: self.rememberMe() || false
		            };

		            authenticationService.login(userInfo, "#/")
		                .then(function() {
		                });
		        };
		    };

		    loginVM.prototype.viewAttached = function (view) {
		        //you can get the view after it's bound and connected to it's parent dom node if you want
		    };

		    return loginVM;
		});
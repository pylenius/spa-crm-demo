﻿define(['services/logger', 'repositories/customers', 'i18n!nls/mainloc'], function (logger, customers, mainloc) {

    // define the viewmodel
    function myViewModel() {
        this.activate = activate;
        this.title = mainloc.customerList;
        this.show = ko.observable(false);
        this.customerList = ko.observableArray();
        this.addCustomer = function () {
            var c = customers.newCustomer();
            this.customerList.push(c);
            c.isSelected(true);
            this.currentPageIndex(this.maxPageIndex());
        };
        this.addNote = function (customer) { customers.addNote(customer); };
        this.revertCustomer = function (customer) {
            customers.cancelChanges(customer);
        };
        this.removeCustomer = function (customer) {
            customers.removeCustomer(customer);
        };
        this.save = function () {
            customers.saveChanges();
            getCustomers();
        };
        this.filter = ko.observable("");
        this.currentPageIndex = ko.observable(0);
        this.pageSize = 5;
        this.itemsOnCurrentPage = ko.computed(function () {
            var startIndex = this.pageSize * this.currentPageIndex();
            return this.customerList.slice(startIndex, startIndex + this.pageSize);
        }, this);
        this.maxPageIndex = ko.computed(function () {
            return Math.ceil(ko.utils.unwrapObservable(this.customerList).length / this.pageSize) - 1;
        }, this);
        this.changedItems = ko.computed(function () {
            var total = 0;
            for (var i = 0; i < ko.utils.unwrapObservable(this.customerList).length; i++)
                if (ko.utils.unwrapObservable(this.customerList)[i].isChanged())
                    total += 1;
            return total;
        }, this);
        this.localize = mainloc;
    };

    var vm = new myViewModel();
    
    // start fetching customers
    getCustomers();

    // re-query when filter input changes
    vm.filter.subscribe(getCustomers);

    return vm;

    //#region private functions

    function activate(context) {
        logger.log('Customer list activated', null, 'home', true);
        return true;
    }
    function getCustomers() {

        customers.getCustomers(vm.filter(), querySucceeded, queryFailed);

        // reload vm.todos with the results 
        function querySucceeded(data) {
            vm.currentPageIndex(0);
            vm.customerList(data);
            vm.show(true); // show the view
        }

        function queryFailed() {
        }
    }

    //#endregion
});
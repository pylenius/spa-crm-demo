﻿define(['durandal/system', 'durandal/plugins/router', 'services/logger', 'i18n!nls/mainloc', 'services/authentication' , "services/facebook"],
    function (system, router, logger, mainloc, authentication, facebook) {
        var shell = {
            activate: activate,
            router: router,
            changeLocale: changeLocale,
            locale: getLocale,
            txtTitle: facebook.name, //mainloc.title,
            isAnonymous: authentication.isAnonymous,
            userInfo: authentication.userInfo,
            authentication: authentication,
            localize: mainloc
        };
        
        return shell;

        //#region Internal Methods
        function activate() {
            return boot();
        }

        function boot() {
            router.mapNav('home', null, mainloc.custmenu);
            router.mapNav('details', null, mainloc.detamenu);
            router.mapRoute('account/login', null, "Login", false);
            router.mapRoute('account/register', null, "Login", false);
            log(mainloc.loaded, null, true);
            return router.activate('home');
        }

        function log(msg, data, showToast) {
            logger.log(msg, data, system.getModuleId(shell), showToast);
        }
        
        function changeLocale(locale) {
            localStorage.setItem('locale', locale);
            location.reload();
        }
        //#endregion
    });
﻿define(['services/logger'], function (logger) {

    return {
        _customerList: ko.observableArray(),
        
        _manager: new breeze.EntityManager('breeze/CustomerData'),
        
        _customerInit: function(entity) {
            if (entity.isSelected === undefined) {
                entity.isSelected = ko.observable(false);
            }

            if (entity.isChanged === undefined) {
                var isUnchanged = entity.entityAspect.entityState.isUnchanged();
                entity.isChanged = ko.observable(!isUnchanged);
            }
        },
        
        getCustomers: function (filter, success, error) {

            var context = this;
            logger.log("querying Customers");

            var query = breeze.EntityQuery.from("Customers").expand("Notes");

            // Include filter
            if (filter.length > 0) {
                var f = breeze.Predicate.create("Name", breeze.FilterQueryOp.Contains, filter)
                                       .or("Street", breeze.FilterQueryOp.Contains, filter);
                query = breeze.EntityQuery.from("Customers").where(f).expand("Notes");
            }
            
            // subscribe with handler watching for EntityState changes
            addEntityStateChangeTracking(this._manager);
            
            return this._manager
                .executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                // Add custom members
                var customers = data.results;
                customers.forEach(function (c) {
                    context._customerInit(c);
                });

                logger.log("queried Customers");
                success(data.results);
            }

            function queryFailed(data) {
                logger.log("Query failed: " + data.message);
                error();
            }

            function addEntityStateChangeTracking(entityManager) {

                if (entityManager._entityStateChangeTrackingToken) {
                    return;
                } // already tracking it

                // remember the change tracking subscription with a token; 
                // might unsubscribe with that token in future
                entityManager._entityStateChangeTrackingToken =
                    entityManager.entityChanged.subscribe(entityChanged);

                var entityStateChangeAction = breeze.EntityAction.EntityStateChange;

                function entityChanged(changeArgs) {
                    if (changeArgs.entityAction === entityStateChangeAction) {
                        var entity = changeArgs.entity;
                        if (entity && entity.isChanged) { // entity has the observable
                            var isUnchanged = entity.entityAspect.entityState.isUnchanged();
                            entity.isChanged(!isUnchanged);
                        }
                    }
                }
            }
        },

        newCustomer: function () {
            var newCust = this._manager.createEntity('CustomerItem', { Name: 'Acme', Street: 'Street1' });
            this._customerInit(newCust);            
            return newCust;
        },

        addNote: function (customer) {
            var newNote = this._manager.createEntity('NoteItem', { Text: 'Note', Added: new Date() });
            customer.Notes().push(newNote);
        },

        cancelChanges: function(customer) {
            customer.entityAspect.rejectChanges();
        },       
        
        removeCustomer: function (customer) {
            customer.entityAspect.rejectChanges();
            customer.entityAspect.setDeleted();
        },

        saveChanges: function () {
            var context = this;
            return this._manager.saveChanges()
                .then(function () {
                    // Mark all unchanged after succesfull save
                    var l = context._manager.getEntities();
                    for (var i = 0; i < l.length; i++) {
                        l[i].isChanged(false);
                    }
                    logger.log("changes saved");
                }, context)
                .fail(function (error) { logger.log("Save failed: " + error.message); });
        }        
    };
});
﻿require.config({
    paths: { "text": "durandal/amd/text" },
    locale: getLocale()
});

    function getLocale() {
        var locale = 'en-us';
        if (localStorage) {
            var storedLocale = localStorage.getItem('locale');
            locale = storedLocale || locale;
        }
        return locale;
    }
    
    define(['durandal/app', 'durandal/viewLocator', 'durandal/system', 'durandal/plugins/router', 'services/logger', 'services/authentication', 'services/antiforgery'],
    function (app, viewLocator, system, router, logger, authentication, antiforgery) {

    // Enable debug message to show in the console 
        system.debug(true);
        
        antiforgery.addAntiForgeryTokenToAjaxRequests();
        authentication.handleUnauthorizedAjaxRequests(function () {
            app.showMessage('You are not authorized, please login')
            .then(function () {
                router.navigateTo('#/user/login');
            });
        });
        
    app.start().then(function () {
        toastr.options.positionClass = 'toast-bottom-right';
        toastr.options.backgroundpositionClass = 'toast-bottom-right';

        router.handleInvalidRoute = function (route, params) {
            logger.logError('No Route Found', route, 'main', true);
        };

        // When finding a viewmodel module, replace the viewmodel string 
        // with view to find it partner view.
        router.useConvention();
        viewLocator.useConvention();
        
        // Adapt to touch devices
        app.adaptToDevice();
        //Show the app by setting the root view model for our application.
        app.setRoot('viewmodels/shell', 'entrance');
    });
        


});
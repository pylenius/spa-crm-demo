﻿using Microsoft.Web.WebPages.OAuth;

namespace CRM_Demo
{
    public static class AuthConfig
    {
        public static void RegisterAuth()
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            //OAuthWebSecurity.RegisterTwitterClient(
            //    consumerKey: "",
            //    consumerSecret: "");

            OAuthWebSecurity.RegisterFacebookClient(
                appId: "165465516809499",
                appSecret: "9c496dacf1e4d78e7bb93553a2695363");

            OAuthWebSecurity.RegisterGoogleClient();
        }
    }
}
